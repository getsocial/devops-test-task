module bitbucket.org/getsocial/devops-test-task

go 1.15

require (
	github.com/kelseyhightower/envconfig v1.4.0
	github.com/neo4j/neo4j-go-driver/v4 v4.2.3
)
