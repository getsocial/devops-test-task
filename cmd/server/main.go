package main

import (
	"encoding/json"
	"log"
	"net/http"
	"strconv"

	"bitbucket.org/getsocial/devops-test-task/neo4j"
	"github.com/kelseyhightower/envconfig"
)

type ServerConfig struct {
	ListenAddress string `split_words:"true" required:"true" default:":8888"`
	Neo4j         *neo4j.Config
}

func main() {
	var c ServerConfig
	log.Print("Processing application configurations...")
	if err := envconfig.Process("gs_task", &c); err != nil {
		log.Fatalln("failed to process configuration:", err)
	}

	log.Print("Initializing connection to Neo4j database...")
	if err := neo4j.Init(c.Neo4j); err != nil {
		log.Fatalln("failed to establish Neo4j connection:", err)
	}

	http.HandleFunc("/item", handlerCreateItem)

	log.Println("Starting web server on address", c.ListenAddress)
	if err := http.ListenAndServe(c.ListenAddress, nil); err != nil {
		log.Fatalln("failed to start http server:", err)
	}
}

func handlerCreateItem(w http.ResponseWriter, req *http.Request) {
	query := req.URL.Query()

	var item *neo4j.Item

	if len(query) > 0 {
		id, err := strconv.Atoi(query.Get("id"))
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)
			json.NewEncoder(w).Encode(map[string]string{
				"error":   `bad "id" value`,
				"details": `"id" must be numeric`,
			})
			return
		}
		item = &neo4j.Item{
			ID:   int64(id),
			Name: query.Get("name"),
		}
	}

	switch req.Method {
	case http.MethodPost:
		if item == nil || item.ID == 0 || item.Name == "" {
			w.WriteHeader(http.StatusBadRequest)
			json.NewEncoder(w).Encode(map[string]string{
				"error":   "request query is missing",
				"details": `"id" and "name" query parameters are required`,
			})
			return
		}

		if err := neo4j.CreateItem(item); err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			w.Write([]byte(err.Error()))
			return
		}
		w.WriteHeader(http.StatusCreated)
		return
	case http.MethodGet:
		items, err := neo4j.GetItem(item)
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			w.Write([]byte(err.Error()))
			return
		}
		w.WriteHeader(http.StatusOK)
		json.NewEncoder(w).Encode(items)
		return
	default:
		w.WriteHeader(http.StatusNotFound)
		return
	}
}
